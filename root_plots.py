
#################################################################################
#       This macro made the plots that you can find into rootfile.root          #
# this plots are: noise_occupancy_proj AND occupancy_proJ AND noise_mask_proj   #
#                           HOW TO RUN: root_plots.py                           #
#################################################################################

import numpy as np
import matplotlib.pyplot as plt
import argparse
import sys 
import ROOT as r
import json
import os

out_folder = '/eos/user/l/lvannoli/TestBeam/august_22_cern/plots/root_plots/'
root_file_dir = '/eos/user/l/lvannoli/TestBeam/august_22_cern/data/threshold_1000e/'
frontends = {'SCC3':['0x139a8','plane111','plane131'],'SCC4':['0x13198','plane110','plane130'],'SCC5':['0x131eb','plane112','plane132']}
labels = {'SCC2':r'W12-G_IRRAD_$1.5\times10^{16}n_{eq}/cm^{2}$','SCC3':r'W12-M_BONN_$1.0\times10^{16}n_{eq}/cm^{2}$','SCC5':r'W12-J_BONN_$1.0\times10^{16}n_{eq}/cm^{2}$','SCC8':r'W14-K_IRRAD_$1.5\times10^{16}n_{eq}/cm^{2}$', 'Q6.1':'Q6.1 unirradiated quad module', 'SCC4':'W12-N unirradiated module'}
marker = ['o','d','x']

r.gROOT.SetBatch(True)

def root_plot_1d(th1f,dut,hv,title):
    h1 = th1f[dut][hv][title]['plot']
    c1 = r.TCanvas("c", "c", 1000,800)
    h1.Draw()
    r.gPad.SetLogy()
    h1.SetTitle(f'{title}')
    plot_name = f'{out_folder}{dut}_{hv}_{title}.pdf'
    c1.Print(plot_name)

def root_plot_2d(th2f,dut,hv,title):
    h2 = th2f[dut][hv][title]['plot']
    c1 = r.TCanvas("c", "c", 1000,800)
    h2.Draw('zcol')
    h2.SetTitle(f'{title}')
    c1.SetRightMargin(0.25)
    c1.SetBottomMargin(0.15)
    c1.SetLeftMargin(0.15)
    h2.GetZaxis().SetTitleOffset(1.2)
    h2.GetXaxis().SetTitleOffset(1.)
    h2.GetYaxis().SetTitleOffset(1.)
    h2.GetXaxis().SetLabelSize(.05)
    h2.GetYaxis().SetLabelSize(.05)
    h2.GetZaxis().SetLabelSize(.05)
    plot_name = f'{out_folder}{dut}_{hv}_{title}.pdf'
    c1.Print(plot_name)

def read_root_file(file_path):
    return r.TFile.Open(file_path)   

def get_th1f(root_files):
    th1f = {}
    for k in frontends.keys():
        th1f.update({k:{}})
        for name, files in root_files.items():
            hv = name.split('/')[-1].split('.')[-5]
            th1f[k].update({hv:{}})

    for k in frontends.keys():
        for name, files in root_files.items():
            hv = name.split('/')[-1].split('.')[-5]
            for key, values in rootfile_maps_names[name.split('/')[-1].split('.')[0][:-4]][k].items():
                th1f[k][hv].update({key:{'plot':files.Get(values['plot_name']),'bins':values['bins'],'min':values['min'],'max':values['max']}})
    return th1f

if __name__ == '__main__':
    
    with open('rootfile_maps_names.json', 'r') as f:
        rootfile_maps_names = json.load(f)

    root_files = {}
    for filename in os.listdir(root_file_dir):
        f = os.path.join(root_file_dir, filename)
        #if os.path.isfile(f) and (filename.startswith('thresholdscan.100V') or filename.startswith('noisescan.2.100V')) and filename.endswith('.root'):
        if os.path.isfile(f) and filename.startswith('noisescan.1') and filename.endswith('.root'):
            root_files.update({f'{filename}':read_root_file(f)})
    
    th1f = get_th1f(root_files)
    for dut, val in th1f.items():
        for hv, v in val.items():
            for title, vv in v.items():
                if title == 'noise_occupancy_proj' or title == 'occupancy_proj' or title == 'noise_mask_proj':
                    root_plot_1d(th1f,dut,hv,title)
                else:
                    root_plot_2d(th1f,dut,hv,title)
                    

