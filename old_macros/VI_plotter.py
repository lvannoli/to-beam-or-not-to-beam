###################################################################
###################### how to run ################################# 
# python3 VI_plotter.py -f Testbeam_DCS_output_file.txt           #
#                                                                 #
#This macro creates VI scan plot from a single SCC VI scan.       #
#Data must be in Testbeam DCS format without the headers line     #
#the resistor value is not computated but given as a parameter (R)#
###################################################################

import numpy as np
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-f',action='store', type=str ,help='the name of the file with IV the data you want to plot')
args = parser.parse_args()
#fin = args[1]
print(args.f)

#data = np.genfromtxt(str(args.f), delimiter='\t', skip_header=3, usecols=(1, 2))
data = np.genfromtxt(str(args.f), delimiter='\t', usecols=(1, 2))
R  = 200000
#m,b = np.polyfit(data[:,0][:10],data[:,1][0:10], 1)
#R = data[:,0][0]/data[:,1][0]
print(f'R = {R}')
#print(b)
v_bias = (data[:,0] - R*data[:,1])*(-1)
data[:,1] = data[:,1]*(-1)/4

[print(f'V_bias = {d}, V_keithley = {data[:,0][i]}, Current = {data[:,1][i]}') for i,d in enumerate(v_bias) if d > 105 and d < 125]


#fig, ax1 = plt.subplots(constrained_layout=True)
fig, ax1 = plt.subplots()
ax1.plot(v_bias, data[:,1]*1000, 'bo', markersize=1.5) 
ax1.set_title('IV Scan '+str(args.f[3:7]))
ax1.set_xlabel('Bias Voltage [V]')
#ax1.xaxis.label.set_color('blue') 
ax1.tick_params(axis='x', colors='black')
ax1.set_ylabel('Leakage Current [mA/cm^2]')
ax1.set_ylim((0,0.06))
ax2 = ax1.twiny()
ax2.plot(data[:,0]*(-1), data[:,1]*1000, alpha=0.0)
ax2.set_xlabel('Keithley Voltage [V]')
ax2.xaxis.label.set_color('red') 
ax2.tick_params(axis='x', colors='red')
#print(v_bias[::3])
ax1.set_xticks(np.linspace(v_bias[0], v_bias[-1], len(v_bias[::10])))
#second_ax.set_xlabel(r"Voltage [V]")
#plt.xticks(np.arange(min(data[:,0]), max(data[:,0]), 5))
#plt.yticks(np.arange(min(data[:,1]), max(data[:,1])+0.1, 0.05))
ax1.grid()
fig.savefig(str(args.f[:-4])+'.png')
plt.show()



