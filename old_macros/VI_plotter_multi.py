###########################################################################
###################### how to run ######################################### 
# python3 VI_plotter.py -f first_file.txt -f secondo_file.txt -f ...      #
#                                                                         #
#This macro creates VI scan plot taking in input also several SCC VI scan.#
#Data must be in Testbeam DCS format without the headers line             #
#the resistor value is not computated but given as a parameter (R)        #
###########################################################################

import numpy as np
import matplotlib.pyplot as plt
import argparse
import math

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-f',action='append', type=str ,help='the name of the file with IV the data you want to plot')
args = parser.parse_args()
files = args.f
print(files)
name = [f.split('/')[-1] for f in files]

folder = 'august_22_cern/'
R  = 200000
data = [np.genfromtxt(str(f), delimiter='\t', skip_header=3, usecols=(1, 2)) for f in files]
Tb = -10+273.15
T = -39

v_bias = [(data[i][:,0] - R*data[i][:,1])*(-1) for i, f in enumerate(data)]
leakage_c = [(data[i][:,1]*(-1))/4 for i, f in enumerate(data)]
leakage_c_temperature = [lc*((248/234)**2)*math.exp((1.2/(2*8.64*10**(-5)))*(1/(234) - 1/(248))) for lc in leakage_c]

def vbias_vkei(vbias_min=0, vbias_max=200):
    #this loop prints the V_bias values you chosen with the if at the corrispondent V_keithle values.
    for j, f in enumerate(files):
        print(f'{name[j][3:7]}')
        for i, d in enumerate(v_bias[j]):
            if (d > vbias_min and d < vbias_max):
                #print(f'{V_bias = {d}, V_keithley = {data[j][:,0][i]}, Current = {data[j][:,1][i]}')
                print(f'V_bias = {d}\t V_keithley = {data[j][:,0][i]}')

def plots(v_bias,leakage_c,title,xlabel,ylabel,x_lim,y_lim):
    
    fig=plt.figure()
    ax = []
    plots = []
    ax.append(fig.add_subplot(111, label=f'{name[0][3:7]}'))
    plots.append(ax[0].plot(v_bias[0], leakage_c[0]*1000, marker='o', color=f'C0',linestyle = 'None', markersize=2, label=f'{name[0][3:7]}'))
    ax[0].set_xlim(x_lim)
    ax[0].set_ylim(y_lim)
    ax[0].set_title(title)
    ax[0].set_xlabel(xlabel, color=f'black')
    ax[0].set_ylabel(ylabel, color=f'black')
    ax[0].tick_params(axis='x', colors='black')
    ax[0].tick_params(axis='y', colors=f'black')
    ax[0].grid()
    for j in range(len(files)-1):
        i = j+1
        ax.append(fig.add_subplot(111, label=f'{name[i][3:7]}', frame_on=False))
        plots.append(ax[i].plot(v_bias[i], leakage_c[i]*1000, marker='o', color=f'C{i}',linestyle = 'None', markersize=2, label=f'{name[i][3:7]}'))
        ax[i].set_xlim((x_lim))
        ax[i].set_ylim((y_lim))
        ax[i].axis('OFF')
        
    fig.legend() 
    fig.savefig(folder+'plots/'+title+'_'.join([n[3:7] for n in name])+'_'+name[0][-7:-4]+'.png')
    print('plots_saved as: '+folder+'plots/'+title+'_'.join([n[3:7] for n in name])+'_'+name[0][-7:-4]+'.png')
    plt.show()
    

if __name__ == "__main__":
    
    vbias_vkei(0,55)
    #print(leakage_c_temperature[0]/leakage_c[0])
    #plots(v_bias, leakage_c, 'IV_Scan_', 'Bias Voltage [V]', 'Leakage Current [mA/cm^2]',[-10,200],[0,0.07])
    #[print(lct*1000) for lct in leakage_c_temperature]
    #plots(v_bias, leakage_c_temperature, 'IV_Scan_@_-25°C_', 'Bias Voltage [V]', 'Leakage Current @ -25°C [mA/cm^2]',[-10,200],[0,0.5]) 
    exit(0)
