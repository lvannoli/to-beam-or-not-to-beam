######################################################################################
################################ HOW TO RUN ########################################## 
#  python3 maps_analyzer.py -t thresholdscan_output -fe SCC3/SCC5/SCC4 (on nothing)  #
#                                                                                    #
#  this macro reads th2fs from a rootfile and genereate plots between two of them.   # 
#                                                                                    #
######################################################################################

import numpy as np
import matplotlib.pyplot as plt
import argparse
import ROOT as r

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-t', action='store', type=str ,help='the path to the thresholdscan rootfile', default = None)
parser.add_argument('-e', action='store', type=str ,help='the path to the file with the efficiency map', default = None)
parser.add_argument('-n', action='store', type=str, help='the path to the noisescan rootfile', default = None)
parser.add_argument('-fe', type=str ,help='which frontend we are going to use', default='all')
args = parser.parse_args()

thr_rootfile = args.t
eff_rootfile = args.e
noise_rootfile = args.n
files = []
if thr_rootfile != None:
    file_pice = thr_rootfile.split('.')
    voltage = [p for p in file_pice if 'V' in p][0]
    files.append(r.TFile.Open(thr_rootfile))
if noise_rootfile != None:
    file_pice = noise_rootfile.split('.')
    voltage = [p for p in file_pice if 'V' in p][0]
    files.append(r.TFile.Open(noise_rootfile))
if eff_rootfile != None:
    file_pice = eff_rootfile.split('.')
    voltage = [p for p in file_pice if 'V' in p][0]
    files.append(r.TFile.Open(eff_rootfile))
    run = [p for p in file_pice if 'run' in p][0]
    run = float(run[-4:])
    print(f'run = {run}')

print(f'voltage = {voltage}')
frontends = {'SCC3':['0x139a8','plane111','plane131'],'SCC4':['0x13198','plane110','plane130'],'SCC5':['0x131eb','plane112','plane132']}
if args.fe == 'all': th2fs_name = {k:{} for k,v in frontends.items()}
else: th2fs_name = {args.fe:{}}
## THRESHOLDSCAN FILE
if thr_rootfile != None:
    if args.fe == 'all':
        for key, val in frontends.items():
            print(th2fs_name)
            th2fs_name[key].update({'threshold_map':str(val[0])+'/ThresholdMap_0/Map','noise_map': str(val[0])+'/NoiseMap_0/Map'})#,'th_vs_tdac': str(val[0])+'/ThresholdMap_0/TDAC'})
    else:
        
        th2fs_name[args.fe].update({'threshold_map':frontends[args.fe][0] +'/ThresholdMap_0/Map','noise_map': frontends[args.fe][0]+'/NoiseMap_0/Map'})#,'th_vs_tdac': frontends[args.fe][0]+'/ThresholdMap_0/TDAC'})
##EFFICIENCY FILE        
if eff_rootfile != None:
    if run<3804:
        if args.fe == 'all':
            for key, val in frontends.items():
                th2fs_name[key].update({'track_eff':val[1]+'/AnalysisEfficiency/'+val[1]+'/chipEfficiencyMap_trackPos_TProfile','cluster_eff': val[1]+'/AnalysisEfficiency/'+val[1]+'/chipEfficiencyMap_clustPos_TProfile'})
        else:
            th2fs_name[args.fe].update({'track_eff':frontends[args.fe][1] +'/AnalysisEfficiency/'+frontends[args.fe][1]+'/chipEfficiencyMap_trackPos_TProfile','clust_eff': frontends[args.fe][1]+'/AnalysisEfficiency/'+frontends[args.fe][1]+'/chipEfficiencyMap_clustPos_TProfile'})
    else:
        if args.fe == 'all':
            for key, val in frontends.items():
                th2fs_name[key].update({'track_eff':val[2]+'/AnalysisEfficiency/'+val[2]+'/chipEfficiencyMap_trackPos_TProfile','cluster_eff': val[2]+'/AnalysisEfficiency/'+val[2]+'/chipEfficiencyMap_clustPos_TProfile'})
        else:
            th2fs_name[args.fe].update({'track_eff':frontends[args.fe][2] +'/AnalysisEfficiency/'+frontends[args.fe][2]+'/chipEfficiencyMap_trackPos_TProfile','clust_eff': frontends[args.fe][2]+'/AnalysisEfficiency/'+frontends[args.fe][2]+'/chipEfficiencyMap_clustPos_TProfile'})
##NOISESCAN FILE
if noise_rootfile != None:
    if args.fe == 'all':
        for key, val in frontends.items():
            print(th2fs_name)
            th2fs_name[key].update({'noise_mask':val[0]+'/NoiseMask/Map','noise_occupancy': val[0]+'/NoiseOccupancy/Map','occupancy': val[0]+'/Occupancy/Map'})
    else:
        th2fs_name[args.fe].update({'noise_mask':frontends[args.fe][0] +'/NoiseMask/Map','noise_occupancy': frontends[args.fe][0]+'/NoiseOccupancy/Map','occupancy': frontends[args.fe][0]+'/Occupancy/Map'})

def py_plots(values, x, y,  dut=args.fe):
    fig, ax = plt.subplots(figsize =(8, 8))
    plt.hist2d(values[x], values[y],bins=[2500,450], cmap=plt.cm.jet, vmin=0, vmax=50)
    ax.set_title(x+' vs '+y+' '+dut)
    ax.set_xlabel(x+' [e]')
    ax.set_ylabel(y+' [e]')
    #ax.set_xlim([0,2000])
    #ax.set_ylim([0,300])
    plt.colorbar()
    plt.savefig('august_22_cern/plots/'+x+'_vs_'+y+'_'+dut+'_'+voltage+'.png')
    plt.show()

if __name__ == "__main__":
    
    if args.fe != 'all':
        #th2fs = {args.fe:{'threshold':frontends[args.fe] +'/ThresholdMap_0/Map','noise': frontends[args.fe]+'/NoiseMap_0/Map','tdac': frontends[args.fe]+'/ThresholdMap_0/TDAC'}}
        th2fs = {key:f.Get(val) for f in files for key, val in th2fs_name[args.fe].items()}
        print(th2fs)
        values = {}       
        for key, val in th2fs.items():
            print(key, val)
            data = [val.GetBinContent(x,y) for x in range(val.GetNbinsX()) for y in range(val.GetNbinsY())]#sto prendendo la colonna
            values.update({key:data})
        print(values.keys())
        #print(f"values {values}") 
        exit(0)
        [py_plots(val, a, b) for i, (key, val) in enumerate(values.items()) for b in values.items[i + 1:]]
        for i, (ka, va) in enumerate(values.items()):
            for kb, vb in enumerate(values.items()):
                py_plots(val, ka, kb)
            
    
    else:
        #thr_tree = r.TFile.Open(thr_rootfile)
        #th2fs = {}
        #for key,val in frontends.items():
        #    th2fs.update({key:{'threshold':val+'/ThresholdMap_0/Map','noise': val+'/NoiseMap_0/Map','tdac': val+'/ThresholdMap_0/TDAC'}})
        maps = {}
        for key, val in th2fs_name.items():
            maps.update({key:{}})
            for k, v in val.items():
                maps[key].update({k:thr_tree.Get(v)})
        #print(maps)
        values = {}
        for key, val in maps.items():
            values.update({key:{}})
            for k, v in val.items():
                data = [v.GetBinContent(x,y) for x in range(v.GetNbinsX()) for y in range(v.GetNbinsY())]#sto prendendo lo colonna
                values[key].update({k:data})
        print(f"values {values}") 
        exit(0)
        for key,val in frontends.items():
            py_plots(values[key], key)
    exit(0)
