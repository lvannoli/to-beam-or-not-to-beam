# -*- coding: utf-8 -*-
######################################################################################
################################ HOW TO RUN ##########################################
#  python3 maps_analyzer.py -t thresholdscan_output -fe SCC3/SCC5/SCC4 (on nothing)  #
#                                                                                    #
#                   you can also use bash_yarr_maps.sh macro                         #
#                                                                                    #
#  this macro reads th2fs from a rootfile and genereate plots between two of them.   # 
#                                                                                    #
######################################################################################

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import argparse
import ROOT as r
import json
import sys
import itertools
import scipy
from scipy import stats
from scipy.stats import norm
matplotlib.style.use('../my_plots.mplstyle')
colors = ["#2d597a",'tab:green',"#C92447","#219ebc","#a8dadc","#f4a261"]

#out_folder = '/eos/user/l/lvannoli/TestBeam/august_22_cern/plots/maps_plots/'
#out_folder = '/eos/user/l/lvannoli/TestBeam/august_22_cern/plots/plot_tesi/'
out_folder = '/eos/user/l/lvannoli/TestBeam/august_22_cern/plots/retuning/'
out_npy = '/eos/user/l/lvannoli/TestBeam/august_22_cern/numpy/'
parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-f', action='append', type=str ,help='the path to the rootfile', default = None)
parser.add_argument('-fe',action='store', type=str ,help='which frontend we are going to use', default='all')
args = parser.parse_args()
#colors = ['#0065BD','#005293','#003359','#DAD7CB','#E37222', '#A2AD00','#98C6EA', '#64A0C8', '#CCCCC6', '#808080', '#000000']
frontends = {'SCC3':['0x139a8','plane111','plane131'],'SCC4':['0x13198','plane110','plane130'],'SCC5':['0x131eb','plane112','plane132']}
labels = {'SCC2':r'W12-G_IRRAD_$1.5\times10^{16}n_{eq}/cm^{2}$','SCC3':r'W12-M_BONN_$1.0\times10^{16}n_{eq}/cm^{2}$','SCC5':r'W12-J_BONN_$1.0\times10^{16}n_{eq}/cm^{2}$','SCC8':r'W14-K_IRRAD_$1.5\times10^{16}n_{eq}/cm^{2}$', 'Q6.1':'Q6.1 unirradiated quad module', 'SCC4':'W12-N unirradiated module'}

with open('rootfile_maps_names.json', 'r') as f:
    rootfile_maps_names = json.load(f)

try: 
    root_file_names = args.f
    root_file = [r.TFile.Open(f) for f in root_file_names]
    #print(f'root_file_names = {root_file_names}\nroot_file = {root_file}')
    root_file_pieces = {f.split('/')[-1].split('.')[0]:{'version':f.split('/')[-1].split('.')[1],'hv':f.split('/')[-1].split('.')[-5],'date':''.join(f.split('/')[-1].split('.')[-4:-1])} for f in root_file_names}
    #print(root_file_pieces)
    [root_file_pieces[key].update({'version':'1'}) for key, value in root_file_pieces.items() for k, v in value.items() if k=='version' if value['version'] == value['hv']]
    #print(root_file_pieces)
except:
    sys.exit('no root file appended')

def get_th2f():
    th2f = {}
    if args.fe != 'all':
        th2f.update({args.fe:{}})
        for names, files in zip(root_file_names, root_file):
            for key, values in rootfile_maps_names[names.split('/')[-1].split('.')[0][:-4]][args.fe].items():
                th2f[args.fe].update({key:{'plot':files.Get(values['plot_name']),'bins':values['bins'],'min':values['min'],'max':values['max']}})
    else:
        for k in frontends.keys():
            th2f.update({k:{}})
            for names, files in zip(root_file_names, root_file):
                for key, values in rootfile_maps_names[names.split('/')[-1].split('.')[0][:-4]][k].items():
                    th2f[k].update({key:{'plot':files.Get(values['plot_name']),'bins':values['bins'],'min':values['min'],'max':values['max']}})
    return th2f

def get_data(th2f):
    values = {}
    #print(th2f)
    for key, val in th2f.items():
        values.update({str(key):{}})
        for k, v in val.items():
            data = [v['plot'].GetBinContent(x,y) for x in range(v['plot'].GetNbinsX()) for y in range(v['plot'].GetNbinsY())]#sto prendendo la colonna
            values[key].update({k:data})
    return values

def py_plots(values,th2f, x_title, y_title, dut=args.fe):
    
    fig, ax = plt.subplots(figsize =(8, 8))
    
    #x_bins = int(th2f[x_title]['bins'])
    #y_bins = int(th2f[y_title]['bins'])
    x_lim = [float(th2f[x_title]['min']),float(th2f[x_title]['max'])]
    y_lim = [float(th2f[y_title]['min']),float(th2f[y_title]['max'])]
    x_space = np.linspace(x_lim[0],x_lim[1], 50)
    y_space = np.logspace(np.log10(y_lim[0]), np.log10(y_lim[1]), 7)
    print(y_lim)
    print(y_space)
    #if x_title == 'threshold_map' or y_title == 'threshold_map': v_max = 50
    #elif y_title == 'noise_occupancy' or x_title == 'noise_occupancy': v_max = 10
    #else:v_max = 2000
    v_max = 10
    histo, xbins, ybins, im = plt.hist2d(values[x_title], values[y_title], bins=(x_space,y_space), range=[x_lim, y_lim], cmap=plt.cm.jet, vmin=0, vmax=v_max, cmin=1)
    #histo, xbins, ybins, im = plt.hist2d(values[x_title], values[y_title], bins=[x_bins, y_bins], range=[x_lim, y_lim], cmap=plt.cm.jet, vmin=0, vmax=v_max)
    
    ax.set_title(x_title+' vs '+y_title+'\n'+labels[dut])
    ax.set_xlabel(x_title)
    ax.set_ylabel(y_title)
    ax.set_yscale('log')
    plt.colorbar()
    fig_save = out_folder+dut+'_'+x_title+'_vs_'+y_title+'_'+root_file_pieces[root_file_names[0].split('/')[-1].split('.')[0]]['hv']+'_'+root_file_pieces[root_file_names[0].split('/')[-1].split('.')[0]]['date']+'.pdf'
    plt.savefig(fig_save)
    print("saved plot: "+fig_save)
    #plt.show()

def get_expectation_from_fit(fun, low_edge, high_edge, params, norm):
    r"""
    Calculate the bin content.
    """
    cdf1 = fun.cdf(low_edge, *params)
    cdf2 = fun.cdf(high_edge, *params)
    prob = cdf2 - cdf1
    return norm*prob

def py_plots_1d(values,th2f,x_title, y_title,x_max, dut=args.fe):
    
    fig, ax1 = plt.subplots(figsize =(10, 8))
    disabled = []
    enabled = []
    disabled.append([v for i, v in enumerate(values[x_title]) if values[y_title][i]==0])
    total_pixel = len(values[x_title])
    print(f"dut = {dut}")
    print(f"total = {total_pixel}")
    dis = np.array(disabled)
    dis = dis[np.nonzero(dis)]
    print(f"disable with zeros = {len(dis)}")
    dis_ratio = len(dis)/total_pixel
    print(f"disabled pixles ratio = {dis_ratio * 100}%")
    enabled.append([v for i, v in enumerate(values[x_title]) if values[y_title][i]!=0])
    enabled = np.array(enabled)
    enabled = enabled[np.nonzero(enabled)]
    
    # create the histograms
    bins = np.linspace(0, np.ceil(np.max(enabled))+1, 50)
    counts_en, bins_en = np.histogram(enabled, bins=bins)
    bc_en = 0.5 * (bins[:-1] + bins[1:])

    # bins = np.linspace(0, np.ceil(np.max(disabled))+1, 50)
    #counts_dis, bins_dis = np.histogram(disabled, bins=bins)
    #bc_dis = 0.5 * (bins[:-1] + bins[1:])
    counts_dis, bins_dis = np.histogram(dis, bins=bins)
    bc_dis = 0.5 * (bins[:-1] + bins[1:])

    ax1.plot(bc_en, counts_en, drawstyle='steps-mid',color=colors[0], lw=2, label=f"enabled pixel")
    ax1.hist(enabled, bins=bins_en, color=colors[0],alpha=0.5)
    mean = np.mean(enabled)
    print(f'mean = {mean}')
    rms = np.std(enabled)
    textstr1 = '\n'.join((r'mean=%.2f' % (mean, ),r'rms=%.2f' % (rms, ),f'disabled = {dis_ratio*100:.3f}%'))
    props = dict(boxstyle='round', facecolor='wheat', alpha=1)
    ax1.text(0.65, 0.95, textstr1, transform=ax1.transAxes, fontsize=18,verticalalignment='top', bbox=props)
    #textstr2 = f"{dut}: {labels[dut]} at {root_file_pieces[root_file_names[0].split('/')[-1].split('.')[0]]['hv']}"
    #props = dict(boxstyle='round', facecolor='white', alpha=0)
    #ax1.text(0,1.25, textstr2, fontsize=14)
    
    ax2 = ax1.twinx()
    #ax2.plot(bc_dis, counts_dis, drawstyle='steps-mid', color=colors[2],alpha=0.4, lw=2, label=f"disabled pixel")
    #ax2.hist(disabled, bins=bins_dis, color=colors[2],alpha=0.3)
    ax2.plot(bc_dis, counts_dis, drawstyle='steps-mid', color=colors[2],alpha=0.4, lw=2, label=f"disabled pixel")
    ax2.hist(dis, bins=bins_dis, color=colors[2],alpha=0.3)
    
    ax1.set_xlabel(f'{x_title[:-4]} [e]')
    ax1.set_ylabel('enabled pixel')
    ax1.set_title(f"{dut}: {labels[dut]} at {root_file_pieces[root_file_names[0].split('/')[-1].split('.')[0]]['hv']}", fontsize=12)
    ax1.set_xlim(0,x_max)
    ax1.set_ylim(0,50000)
    ax2.set_ylim(0.5, 10e4)
    ax2.set_yscale('log')
    ax2.set_ylabel('disabled pixel')
    ax2.tick_params(axis='y',colors=colors[2])
    ax1.grid()
    ax1.legend(frameon=False, loc="upper left", bbox_to_anchor=(0,1.19), columnspacing=-3.5, handletextpad=0.5, fontsize=16)
    ax2.legend(frameon=False, loc="upper left", bbox_to_anchor=(0,1.25), columnspacing=-3.5, handletextpad=0.5, fontsize=16)
    plt.tight_layout()
    #fig.legend(facecolor="white", framealpha=1)
    
    fig_save_pdf = out_folder+dut+'_'+x_title+'_non_zero_distribution_enabled_and_disabled_pixels_v'+root_file_pieces[root_file_names[0].split('/')[-1].split('.')[0]]['version']+'_'+root_file_pieces[root_file_names[0].split('/')[-1].split('.')[0]]['hv']+'_'+root_file_pieces[root_file_names[0].split('/')[-1].split('.')[0]]['date']+'.pdf'
    fig_save_png = out_folder+dut+'_'+x_title+'_non_zero_distribution_enabled_and_disabled_pixels_v'+root_file_pieces[root_file_names[0].split('/')[-1].split('.')[0]]['version']+'_'+root_file_pieces[root_file_names[0].split('/')[-1].split('.')[0]]['hv']+'_'+root_file_pieces[root_file_names[0].split('/')[-1].split('.')[0]]['date']+'.png'
    
    plt.savefig(fig_save_pdf)
    plt.savefig(fig_save_png)
    print("saved plot: "+fig_save_pdf)
    
    #plt.show()

def py_plot_proj(values,th2f,title, dut):
    print(values[title])

if __name__ == '__main__':  
    th2f = get_th2f()
    values = get_data(th2f)
    ''' 
    for key, val in values.items():
        for pair in itertools.combinations(val.keys(), 2):
            #print(pair)
            if 'proj' not in pair[0] and 'proj' not in pair[1]:
                if 'noise_occupancy' in pair and 'threshold_map' in pair:
                    py_plots(values[key],th2f[key],pair[0],pair[1],key) 
              
    for key, val in values.items():
        py_plot_proj(values[key],th2f[key],'noise_occupancy',key)
    '''
    #threshold distribution of enabled and disabled pixels
    #noise distribution of enabled and disabled pixels
    for key, val in values.items():
        py_plots_1d(values[key],th2f[key],'noise_map','noise_mask',300,key)
        py_plots_1d(values[key],th2f[key],'threshold_map','noise_mask',2000,key)
        
    exit(0)
    
