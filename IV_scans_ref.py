#############################################################################
#                       This macro plots IV scans                           #
#   you can also rescale the Leakage current after temperature rescale      #
#               HOW TO RUN: IV_scans_ref.py -f /datafile.txt                #
#############################################################################
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import argparse
import math
matplotlib.style.use('../my_plots.mplstyle')

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-f',action='append', type=str ,help='the name of the file with IV the data you want to plot')
parser.add_argument('-ref',action='append', type=str,help='the name of the file with the IV data at the temperature of reference', default = None)
parser.add_argument('-rescale',action='store', type=float, help='temperature with which you want to rescale the IV scan', default = None)
parser.add_argument('-resonly',action='store_true', help='with this flag you are plotting the rescaled curve only', default = False)

args = parser.parse_args()
data_files = args.f
ref_files = args.ref
data_file_name = [f.split('/')[-1] for f in data_files]
out_folder = '/eos/user/l/lvannoli/TestBeam/august_22_cern/plots/plot_tesi/'
t_rescaled = args.rescale
R = 200000
data = [np.genfromtxt(f, skip_header=3, usecols=(2, 3), dtype=None, encoding=None) for f in data_files]
'''
names = {'cards':[],'date':[],'temp':[]}
for n in data_file_name:
    name_pieces = n.split('.')
    names['cards'].append(name_pieces[1])
    names['date'].append(name_pieces[2])
    names['temp'].append(name_pieces[-2])
'''
#print(data)
#exit(0)
markers=['o','d','x']
labels = {'SCC2':r'W12-G_IRRAD_$1.68\times10^{16}n_{eq}/cm^{2}$','SCC3':r'W12-M_BONN_$1.0\times10^{16}n_{eq}/cm^{2}$','SCC5':r'W12-J_BONN_$1.0\times10^{16}n_{eq}/cm^{2}$','SCC8':r'W14-K_IRRAD_$1.68\times10^{16}n_{eq}/cm^{2}$', 'Q6.1':'Q6.1 unirradiated quad module', 'SCC4':'W12-N unirradiated module'}

def v_bias(d):
    #print(f'v_bias = {(d[:,0] - R*d[:,1])*(-1)}')
    return (d[:,0] - R*d[:,1])*(-1)
def leakage_c(d):
    #print(f'lc = {(d[:,1]*(-1))/4}')
    return (d[:,1]*(-1))/4
def lc_norm(lc, t_ref, t):
    #print(t_ref, t)
    Tb = t_ref+273.15
    T = t+273.15
    #print(f'lc = {lc*((Tb/T)**2)*math.exp((1.2/(2*8.64*10**(-5)))*(1/(T) - 1/(Tb)))}')
    return lc*((Tb/T)**2)*math.exp((1.12/(2*8.64*10**(-5)))*(1/T - 1/Tb))

if __name__ == "__main__":
    fig = plt.figure(figsize=(14,8))
    counter = 0
    ax = []
    plots = []
    xlabel='Bias voltage [V]'
    ylabel=r"Leakage current [$\frac{\mu A}{cm^{2}}$]"
    y_lim=[-10,700]
    x_lim=[-1,250]
    k = 0
    for i in range(len(data_files)):
        print(f"IV scans of {data_file_name[i][3:7]} taken at the teperature of {data_file_name[i].split('.')[-2]}")
        if i==0:
            ax.append(fig.add_subplot(111, label=f"{data_file_name[i][3:7]}_{data_file_name[i].split('.')[-2]}"))
        else:
            ax.append(fig.add_subplot(111, label=f"{data_file_name[i][3:7]}_{data_file_name[i].split('.')[-2]}", frame_on=False))
        plots.append(ax[i].plot(v_bias(data[i]), leakage_c(data[i])*1000000, marker=markers[0], color=f'C{i}',linestyle = 'None', markersize=8, label=f"{labels[data_file_name[i][3:7]]} at {data_file_name[i].split('.')[-2]}"))
        ax[i].set_xlim((x_lim))
        ax[i].set_ylim((y_lim))
        counter += 1
        k += 1
    ax[0].grid(True, which='both', ls='-')
    ax[0].set_xlabel(xlabel, color=f'black')
    ax[0].set_ylabel(ylabel, color=f'black')
    ax[0].tick_params(axis='x', colors='black')
    ax[0].tick_params(axis='y', colors=f'black')
    if args.ref != None:
        ref_file_name = [f.split('/')[-1] for f in ref_files]
        ref = [np.genfromtxt(f, skip_header=3, usecols=(2, 3), dtype=None, encoding=None) for f in ref_files]
        for j in range(len(ref_files)):
            ax.append(fig.add_subplot(111, label=f"{ref_file_name[j][3:7]}_ref_{ref_file_name[j].split('.')[-2]}", frame_on=False))
            plots.append(ax[counter].plot(v_bias(ref[j]), leakage_c(ref[j])*1000000, marker=markers[0], color=f'C{counter}',linestyle = 'None', markersize=8, label=f"{labels[ref_file_name[j][3:7]]} at {ref_file_name[j].split('.')[-2]}"))
            ax[counter].set_xlim(x_lim)
            ax[counter].set_ylim(y_lim)
            ax[counter].axis('off')
            counter += 1
    if args.rescale != None:
        title=f'IV scan irradiated modules'
        plot_name = f"{title} leakage current rescaled at {t_rescaled}"
        for j in range(len(data_files)):
            print(f"rescaling IV scans taken at the temperature of {float(data_file_name[j].split('.')[-2][1:-1])} to the temperature of {t_rescaled}")
            if args.resonly == True and counter == len(data_files):
                ax.append(fig.add_subplot(111, label=f"{data_file_name[j][3:7]}_res_{data_file_name[j].split('.')[-2]}"))
            else:
                ax.append(fig.add_subplot(111, label=f"{data_file_name[j][3:7]}_res_{data_file_name[j].split('.')[-2]}", frame_on=False))
            plots.append(ax[counter].plot(v_bias(data[j]), lc_norm(leakage_c(data[j]), t_rescaled, float(data_file_name[j].split('.')[-2][0:-1]))*1000000, marker=markers[0], fillstyle='none', markeredgecolor=f'C{j}',linestyle = 'None', markersize=8, label=f'{labels[data_file_name[j][3:7]]} rescaled at {t_rescaled}'))
            #print(v_bias(data[j]),': ',lc_norm(leakage_c(data[j]), t_rescaled, float(data_file_name[j].split('.')[-2][0:-1]))*1000)
            ax[counter].set_xlim((x_lim))
            ax[counter].set_ylim((y_lim))
            if args.resonly == False: ax[counter].axis('off')
            counter += 1       
    else: 
        title='IV scan irradiated modules'
        plot_name = f"{title}" 
    log = ''
    #for i in range(counter):
    #   ax[i].set_yscale('log')    
    #   log = '_log'    
    #fig.legend(loc=(0.15,0.32),facecolor='white',framealpha=1) 
    if args.resonly == True:
        for kk in range(k):
            ax[kk].cla()
            ax[kk].axis('off')
        for c in range(k, counter-1):
            ax[c+1].axis('off')
        ax[k].grid(True, which='both', ls='-')
        ax[k].set_xlabel(xlabel, color=f'black')
        ax[k].set_ylabel(ylabel, color=f'black')
        ax[k].tick_params(axis='x', colors='black')
        ax[k].tick_params(axis='y', colors=f'black')

    fig.legend(loc=(0.15,0.65), prop={'size': 10.5})
    modules = ''
    for i in data_file_name:
        if i[3:7] not in modules:
            modules += i[3:7]+'_'
    fig.savefig(out_folder+'_'.join(plot_name.split(' '))+'_'+modules+data_file_name[0].split('.')[2]+'_muA'+log+'.pdf')
    print(out_folder+'_'.join(plot_name.split(' '))+'_'+modules+data_file_name[0].split('.')[2]+'_muA'+log+'.pdf')
    plt.show()
