#############################################################################################
#                        HOW TO RUN: python3 noise_vs_voltage.py                            #
#               The routine takes data directly from the root_file_dir                      #
#        into that folder you have to move the rootfile.root produced with plotFromDir      #
#       rootfile.root have to chage their name in scan_type.version.hvV.dd.mm.yyyy.root     #
#       for superimposition of tuned and retuned scc, is_retuned (boolean) has to be set    #
#        (eg. noisescan.2.140V.24.08.2022.root or thresholdscan.1.160V.24.08.2022.root)     #
#          this routine produce noise vs bias voltage plot and save it in out_folder        #
#############################################################################################
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import argparse
import ROOT as r
import json
import os
import sys
matplotlib.style.use('/eos/user/l/lvannoli/my_plots.mplstyle')

out_folder = '/eos/user/l/lvannoli/TestBeam/august_22_cern/plots/retuning/'
root_file_dir = '/eos/user/l/lvannoli/TestBeam/august_22_cern/data/threshold_1000e/'
#the front-ends are taken from the dictionary frontends. You have to update with new frontends
frontends = {'SCC3':['0x139a8','plane111','plane131'],'SCC4':['0x13198','plane110','plane130'],'SCC5':['0x131eb','plane112','plane132']}
#labels are a dictionary with the SCC card name and its sensor name with the irradiation values
labels = {'SCC2':r'W12-G_IRRAD_$1.5\times10^{16}n_{eq}/cm^{2}$','SCC3':r'W12-M_BONN_$1.0\times10^{16}n_{eq}/cm^{2}$','SCC5':r'W12-J_BONN_$1.0\times10^{16}n_{eq}/cm^{2}$','SCC8':r'W14-K_IRRAD_$1.5\times10^{16}n_{eq}/cm^{2}$', 'Q6.1':'Q6.1 unirradiated quad module', 'SCC4':'W12-N unirradiated module'}
marker = ['o','d','x','o','d']
#colors = ["#2d597a",'tab:green',"#219ebc","#a8dadc","#f4a261","#C92447"]
colors = ["#2d597a",'tab:green',"#C92447","#219ebc","#a8dadc","#f4a261"]
is_retuned = True

def root_plot(th1f):
    h1 = th1f['SCC3']['120V']['noise_occupancy_proj']['plot']
    c1 = r.TCanvas()
    h1.Draw()
    c1.Print(out_folder+'SCC3_120_noise_occupancy_proj.png')
    print(out_folder+'SCC3_120_noise_occupancy_proj.png')

def read_root_file(file_path):
    return r.TFile.Open(file_path)   

def get_th1f(root_files):
    th1f = {}
    for k in frontends.keys():
        th1f.update({k:{}})
        for name, files in root_files.items():
            hv = name.split('/')[-1].split('.')[-5]
            try:
                th1f[k].update({hv:{}})
            except:
                break
            for key, values in rootfile_maps_names[name.split('/')[-1].split('.')[0][:-4]][k].items():
                if key == 'noise_occupancy_proj':# or key == 'occupancy_proj' or key == 'noise_mask_proj':
                    try: 
                        print(files.Get(values['plot_name']).GetName())
                        th1f[k][hv].update({key:{'plot':files.Get(values['plot_name']),'bins':values['bins'],'min':values['min'],'max':values['max']}})
                    except:
                        del th1f[k]
                        print(f"{k} not in root_file named {name}\n{k} removed from the dictionary th1f")
    return th1f

def get_data(hv, vv):
    bins_content = [[float(hv[:-1]), vv['plot'].GetBinContent(x)] for x in range(vv['plot'].GetNbinsX())]
    print(f'bins_content = {bins_content}')
    print(f'total_pixel = {sum(d[1] for d in bins_content[:])}')
    noisy_pixels = sum(d[1] for d in bins_content[3:]) #OCCUPANCY >= 10^-6
    print(f'noisy_pixel = {noisy_pixels}')
    data = [bins_content[0][0],noisy_pixels]
    return data

def py_plot(values, values_retuned=None):
    fig, ax = plt.subplots()
    i = 0
    for keys, val in values.items():
        for k, v in val.items():
            #print(k,v)
            v = sorted(v,key=lambda x: (x[0],x[1]))
            x = [d[0] for d in v]
            y = [d[1]/153600 * 100 for d in v]
            if keys != 'SCC4':
                ax.plot(x,y, marker=marker[i], color = f'{colors[i]}', linestyle ='-', markersize = 6, label = f'{keys} - {labels[keys]}')
            else:
                ax.plot([0,10,20],y[:3], marker=marker[i], color = f'{colors[i]}', linestyle ='-', markersize =6, label = f'{keys} - {labels[keys]}')
            i+=1
    if values_retuned != None: 
        for keys, val in values_retuned.items():
            for k, v in val.items():
                #print(k,v)
                v = sorted(v,key=lambda x: (x[0],x[1]))
                x = [d[0] for d in v]
                y = [d[1]/153600 * 100 for d in v]
                if keys != 'SCC4':
                    ax.plot(x,y, marker=marker[i], color=f'{colors[i]}', linestyle='-', markersize=6, label=f'{keys} - RETUNED_{labels[keys]}')
                else:
                    ax.plot([0,10,20],y[:3], marker=marker[i], color=f'{colors[i]}', linestyle='-', markersize=6, label=f'{keys} - RETUNED_{labels[keys]}')
                i+=1
    print(i)
    ax.grid()
    #ax.set_title(f'noisy pixels vs hv')
    ax.set_xlabel('$V_{Bias}$ [V]')
    ax.set_ylabel('Noisy pixels removed percentage [%]\n(Noise Occupancy $\geq 10^{-6})$')
    ax.legend(facecolor = 'white', framealpha=1)
    ax.tick_params(direction="in", right=True, bottom=True, top=True, left=True, labeltop=True, labelright=True, length=8)
    fig.savefig(out_folder+'not_retuned_thr_1000e_noisy_pixels_vs_hv.pdf')
    fig.savefig(out_folder+'not_retuned_thr_1000e_noisy_pixels_vs_hv.png')
    print(f'plot saved as {out_folder}not_retuned_thr_1000e_noisy_pixels_vs_hv.pdf')
    plt.show()

if __name__ == '__main__':
    
    isExist = os.path.exists(out_folder)
    if not isExist:
      # Create a new directory because it does not exist 
        os.makedirs(out_folder)
        print(f"{out_folder} was created")

    with open('rootfile_maps_names.json', 'r') as f:
        rootfile_maps_names = json.load(f)

    root_files = {}
    root_files_retuned = {}
    for filename in os.listdir(root_file_dir):
        f = os.path.join(root_file_dir, filename)
        if os.path.isfile(f) and filename.startswith('noisescan.1.') and filename.endswith('.root'):
            root_files.update({f'{filename}':read_root_file(f)})
        elif is_retuned and os.path.isfile(f) and filename.startswith('noisescan.1_2') and filename.endswith('.root'):
            root_files_retuned.update({f'{filename}':read_root_file(f)})
                        
    th1f = get_th1f(root_files)
    values = {}
    for key, val in th1f.items():
        values.update({key:{}})
        data = []
        for k, v in val.items():
            for kk, vv in v.items():
                data.append(get_data(k,vv))
        values[key].update({kk:data})
    print(values)

    if is_retuned == True:
        th1f_retuned = get_th1f(root_files_retuned)
        values_retuned = {}
        for key, val in th1f_retuned.items():
            values_retuned.update({key:{}})
            data = []
            for k, v in val.items():
                for kk, vv in v.items():
                    data.append(get_data(k,vv))
            values_retuned[key].update({kk:data})
#        py_plot(values, values_retuned) 
#    else: py_plot(values)
    exit(0)
